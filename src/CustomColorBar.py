from PySide2.QtCore import QRect, Qt
from PySide2.QtGui import QPainter, QLinearGradient, QFont
from PySide2.QtWidgets import QLabel


class CustomColorBar(QLabel):
    textTop = ""
    textBottom = ""

    def paintEvent(self, event):
        super().paintEvent(event)
        painter = QPainter(self)
        gradient = QLinearGradient(0, 0, 0, self.height())
        gradient.setColorAt(1, Qt.blue)
        gradient.setColorAt(0.75, Qt.green)
        gradient.setColorAt(0.5, Qt.yellow)
        gradient.setColorAt(0.25, Qt.red)
        gradient.setColorAt(0, Qt.white)
        painter.fillRect(QRect(0, 0, 24, self.height()), gradient)

        painter.setFont(QFont("arial", 8))

        painter.setPen(Qt.black)
        painter.drawText(QRect(1, 2, 30, 20), self.textTop)
        painter.setPen(Qt.white)
        painter.drawText(QRect(1, self.height() - 12, 30, 20), self.textBottom)

    def changeColorBar(self, textTop, textBottom):
        self.textTop = textTop
        self.textBottom = textBottom
