from PySide2.QtGui import QPixmap, QIcon, QPainter, Qt, QCursor
from PySide2.QtCore import QSize
from PySide2.QtWidgets import QLabel




styleSheet = """
#mainWidget {background: white; margin: 0; border: 0; }
#Scrollview {background: white; border: 0px; border-left: 1px solid black}
#ScrollviewWidget {background: white; border: 0;}
QScrollBar:vertical {           
    background:white;
    width: 8px; 
    margin: 0px 0px 0px 0px;
}
QScrollBar::handle:vertical {
    background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
    stop: 0 rgb(129, 134, 139), stop: 0.5 rgb(129, 134, 139), stop:1 rgb(129, 134, 139));
    width: 8px;
    min-height: 0px;
    margin-right: 2px;
    }
QScrollBar::add-line:vertical {
    background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
    stop: 0 rgb(129, 134, 139), stop: 0.5 rgb(129, 134, 139),  stop:1 rgb(129, 134, 139));
    height: 0px;
    subcontrol-position: bottom;
    subcontrol-origin: margin;
}
QScrollBar::sub-line:vertical {
    background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
    stop: 0  rgb(129, 134, 139), stop: 0.5 rgb(129, 134, 139),  stop:1 rgb(129, 134, 139));
    height: 0 px;
    subcontrol-position: top;
    subcontrol-origin: margin;
}
"""
iconButtonStyles = """
QPushButton{
    border:none;
    background-color:rgba(255, 255, 255,100);
}
QPushButton::hover {
    background: rgb(200, 200, 200);
}
"""


def getIconInColor(iconPath, color):
    img = QPixmap(iconPath)
    painter = QPainter(img)
    painter.setCompositionMode(QPainter.CompositionMode_SourceIn)
    painter.fillRect(img.rect(), color)
    painter.end()
    return QIcon(img)


def setIconButtonStyle(button, tooltip):
    button.setStyleSheet(iconButtonStyles)
    button.setIconSize(QSize(25, 25))
    button.setMinimumSize(25, 25)
    button.setMaximumSize(25, 25)
    button.setCursor(QCursor(Qt.PointingHandCursor))
    button.setToolTip(tooltip)


def getHelpIcon(tooltip):
    im = QPixmap(":gui_resources/helpIcon.png").scaled(20, 20, Qt.KeepAspectRatio, Qt.SmoothTransformation)
    help_icon = QLabel()
    help_icon.setPixmap(im)
    help_icon.setToolTip(tooltip)
    return help_icon



