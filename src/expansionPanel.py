from PySide2.QtCore import QPointF, SIGNAL, QObject, QPoint, Qt
from PySide2.QtWidgets import QWidget, QHBoxLayout, QFrame, QVBoxLayout, QLabel
from PySide2.QtGui import QPainter, QColor


class ExpansionPanel(QWidget):
    def __init__(self, parent=None, title=None):
        super().__init__(parent)
        self._is_collasped = True
        self._title_frame = None
        self._content, self._content_layout = (None, None)

        self._main_v_layout = QVBoxLayout(self)
        self._main_v_layout.setSpacing(0)
        self._main_v_layout.addWidget(self.initTitleFrame(title, self._is_collasped))
        self._main_v_layout.addWidget(self.initContent(self._is_collasped))

        self.initCollapsable()

    def initTitleFrame(self, title, collapsed):
        self._title_frame = self.TitleFrame(title=title, collapsed=collapsed)

        return self._title_frame

    def initContent(self, collapsed):
        self._content = QWidget()
        self._content.setStyleSheet("*[styled_grey='true'] {border-bottom:1px solid rgb(41, 41, 41);"
                                    " border-left:1px solid rgb(41, 41, 41); border-right:1px solid rgb(41, 41, 41);"
                                    " background-color: rgb(238,238,238); margin-bottom: 2px}")
        self._content.setProperty("styled_grey", True)
        self._content_layout = QVBoxLayout()

        self._content.setLayout(self._content_layout)
        self._content.setVisible(not collapsed)

        return self._content

    def addWidget(self, widget):
        self._content_layout.addWidget(widget)

    def addLayout(self, layout):
        self._content_layout.addLayout(layout)

    def addTitleIconButton(self, button):
        self._title_frame.title_bar_button_layout.addWidget(button)

    def initCollapsable(self):
        QObject.connect(self._title_frame, SIGNAL('clicked()'), self.toggleCollapsed)

    def toggleCollapsed(self):
        self._content.setVisible(self._is_collasped)
        self._is_collasped = not self._is_collasped
        self._title_frame._arrow.setArrow(int(self._is_collasped))
        self._title_frame.setStyle(self._is_collasped)

    def set_Title(self, title):
        self._title_frame._title.setText(title)

    ############################
    #           TITLE          #
    ############################
    class TitleFrame(QFrame):
        def __init__(self, parent=None, title="", collapsed=False):
            QFrame.__init__(self, parent=parent)

            self.setMinimumHeight(24)
            self.move(QPoint(24, 0))
            self.setStyle(collapsed)

            self._hlayout = QHBoxLayout(self)
            self._hlayout.setContentsMargins(0, 0, 0, 0)
            self._hlayout.setSpacing(0)

            self._arrow = None
            self._title = None


            self.title_bar_button_layout = QHBoxLayout()
            self.title_bar_button_layout.setContentsMargins(0, 0, 10, 0)
            self.title_bar_button_layout.setAlignment(Qt.AlignRight)

            self._hlayout.addWidget(self.initArrow(collapsed))
            self._hlayout.addWidget(self.initTitle(title))
            self._hlayout.addLayout(self.title_bar_button_layout)

        def initArrow(self, collapsed):
            self._arrow = ExpansionPanel.Arrow(collapsed=collapsed)
            self._arrow.setStyleSheet("border:0px")
            return self._arrow

        def initTitle(self, title=None):
            self._title = QLabel(title)
            self._title.setMinimumHeight(24)
            self._title.move(QPoint(24, 0))
            self._title.setStyleSheet("border:0px; background: transparent")
            return self._title

        def mousePressEvent(self, event):
            self.emit(SIGNAL('clicked()'))
            return super(ExpansionPanel.TitleFrame, self).mousePressEvent(event)

        def setStyle(self, collapse):
            if collapse:
                self.setStyleSheet(
                    "border:1px solid rgb(41, 41, 41); background-color: white; border-top-left-radius: 5px;"
                    "border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px")
            else:
                self.setStyleSheet(
                    "border:1px solid rgb(41, 41, 41); background-color: white; border-top-left-radius: 5px;"
                    " border-top-right-radius: 5px")

    #############################
    #           ARROW           #
    #############################
    class Arrow(QFrame):
        def __init__(self, parent=None, collapsed=False):
            QFrame.__init__(self, parent=parent)
            self.setMaximumSize(24, 24)
            # horizontal == 0
            self._arrow_horizontal = (QPointF(7.0, 8.0), QPointF(17.0, 8.0), QPointF(12.0, 13.0))
            # vertical == 1
            self._arrow_vertical = (QPointF(8.0, 7.0), QPointF(13.0, 12.0), QPointF(8.0, 17.0))
            # arrow
            self._arrow = None
            self.setArrow(int(collapsed))

        def setArrow(self, arrow_dir):
            if arrow_dir:
                self._arrow = self._arrow_vertical
            else:
                self._arrow = self._arrow_horizontal

        def paintEvent(self, event):
            painter = QPainter()
            painter.begin(self)
            painter.setBrush(QColor(64, 64, 64))
            painter.setPen(QColor(64, 64, 64))
            painter.drawPolygon(self._arrow)
            painter.end()
