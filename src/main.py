import sys
import rc_images
import json
import numpy as np

from PySide2.QtCore import Qt, QTimer, Signal, QThread, QUrl
from PySide2.QtGui import QPixmap, QImage, QIcon, QDesktopServices
from PySide2.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QFileDialog, QLabel, QLineEdit, \
    QMessageBox, QPushButton, QComboBox, QScrollArea, QProgressDialog, QInputDialog

from qtrangeslider import QLabeledRangeSlider

import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from enum import Enum, auto

from expansionPanel import ExpansionPanel
from drawLabel import DrawLabel
from CustomColorBar import CustomColorBar

from backend import BACKEND
from bamconstants import styleSheet, getIconInColor, setIconButtonStyle, getHelpIcon

backend = BACKEND()


class VideoButtons(Enum):
    PLAY = auto()
    STOP = auto()
    PAUSE = auto()
    SINGLE_FW = auto()
    SINGLE_BW = auto()


class FileLoader(QThread):
    finished = Signal()
    progress = Signal(int)
    max = Signal(int)

    def __init__(self):
        QThread.__init__(self)
        self.filename = ""
        self._isRunning = False

    def run(self):
        self._isRunning = True

        for i in backend.load(self.filename):
            if not self._isRunning:
                return
            self.progress.emit(i[0])
            self.max.emit(i[1])
        self.finished.emit()

    def stop(self):
        self._isRunning = False


class Exporter(QThread):
    finished = Signal()
    progress = Signal(int)
    max = Signal(int)

    def __init__(self):
        QThread.__init__(self)
        self._isRunning = False
        self.dir_name = ""
        self.time = (None, None)
        self.asOne = False

    def run(self):
        self._isRunning = True
        for i in backend.export(self.dir_name, self.time[0], self.time[1], self.asOne):
            if not self._isRunning:
                return
            self.progress.emit(i[0])
            self.max.emit(i[1])
        self.finished.emit()

    def stop(self):
        self._isRunning = False


class AppWidget(QWidget):
    valSignal = Signal(int)
    maxSignal = Signal(int)

    def __init__(self):
        super().__init__()
        # Main window
        self.mainLayout = QHBoxLayout()
        self.mainLayout.addWidget(self.createSettingsMenu(), 1)
        self.mainLayout.addWidget(self.createVideoMenu(), 2)
        # Layout
        self.layout = QVBoxLayout(self)
        self.layout.setContentsMargins(5, 0, 0, 0)
        self.layout.addLayout(self.createTopMenuBar())
        self.layout.addLayout(self.mainLayout)
        # initialize the video frames
        self.direction = 1
        self.of_colorDirection = "y"
        self.strain_colorDirection = "y"
        self.frame = 0
        self.playing = VideoButtons.PAUSE
        self.timer = QTimer()
        self.timer.setInterval(1000 // 50)
        self.timer.start()
        self.timer.timeout.connect(self.updateTimer)
        self.updateRoiAllowed = False
        self.updateHistAllowed = False
        self.roi = None

    # load settings of an old project from txt file with json structure
    # marker
    def loadProject(self, filename):
        with open(filename, 'r') as file:
            obj = json.loads(file.read())

        self.setTitle(obj["project_Title"])

        if obj["path"] != '':
            self.loadfiles(obj["path"])

        self.thread.finished.connect(lambda: self.loadProject_params(obj))

    def loadProject_params(self, obj):
        roi_i = obj['ROI_window']['coordinates']
        self.rp_x_edit.setText(str(roi_i[0]))
        self.rp_y_edit.setText(str(roi_i[1]))
        self.rs_x_edit.setText(str(roi_i[2] - roi_i[0]))
        self.rs_y_edit.setText(str(roi_i[3] - roi_i[1]))
        self.label_original.setRectangle(roi_i[0], roi_i[1], roi_i[2], roi_i[3])
        self.roi = [roi_i[0], roi_i[1], roi_i[2], roi_i[3]]
        self.updateWindow()
        self.updateRoiAllowed = True
        self.tr_slider.setValue((int(obj["timerange"][0]), int(obj["timerange"][1])))

        """filter sind momentan im BE ausgebaut
        filter_p = obj['raw_filter']['parameter']
        self.rf_dropdown.setCurrentText(obj['raw_filter']['selected'])
        self.rf_param_size.setCurrentText(filter_p['Size'])
        self.send_filter_params()"""

        of_f = dict(obj["filter_of"]["parameter"])
        print(of_f['Size'])
        self.of_f_param_size.setCurrentText(of_f['Size'])

        of_p = dict(obj["optical_flow"]["parameter"])
        self.of_window.setCurrentText(of_p['winSize'])
        self.of_max_lvl.setCurrentText(of_p['maxLevel'])
        self.of_epsilon.setText(str(of_p['epsilon']))
        self.of_count.setText(str(of_p['count']))
        self.of_akku.setText(str(of_p['accumulation']))
        self.send_opt_flow_params()

        self.p_x_edit.setText(str(obj['historical_point'][0]))
        self.p_y_edit.setText(str(obj['historical_point'][1]))
        self.label_original.setHistPoint(obj['historical_point'][0], obj['historical_point'][1])

    def filedialogue(self):
        filename = QFileDialog.getOpenFileName(None, 'Test Dialog', '', 'All Files(*.*)')
        if filename[0] == '' and filename[1] == '':
            return
        if filename[0].endswith('.txt'):
            self.loadProject(filename[0])
        else:
            self.loadfiles(filename[0])

    def loadfiles(self, filename):
        self.progressDialog = QProgressDialog("Loading Raw Data...", "Cancel", 0, 100, self)
        self.thread = QThread()
        self.worker = FileLoader()
        self.worker.filename = filename
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)

        def cancel():
            self.worker.stop()
            self.thread.quit()
            

        self.progressDialog.canceled.connect(cancel)
        self.worker.progress.connect(self.progressDialog.setValue)
        self.worker.max.connect(self.progressDialog.setMaximum)
        self.thread.start()
        self.thread.finished.connect(lambda: self.loadFiles(filename))

    def loadFiles(self, filename):
        self.updateWindow()
        self.frame = 0
        self.setVideoPause()

        self.projectLocation.setText(filename)
        self.progressDialog.close()

        self.tr_slider.setMinimum(0)
        self.tr_slider.setMaximum(backend.getImgCount())
        self.tr_slider.setValue((0, backend.getImgCount()))

    def plot(self):
        self.figure.clear()
        ax = self.figure.add_subplot(111)
        ax.plot([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], '*-')
        self.canvas.draw()

    ''''currently not used, at all
    # rohdatenfilter 
    def rf_params(self):
        # delete all widgets, else they stack up whenever we change the dropdown
        for i in reversed(range(len(self.rf_widgets))):
            self.rf_widgets[i].setParent(None)
            del self.rf_widgets[-1]
        filters = backend.getFilters()
        filter = backend.getFilter()
        params = filters[filter].getParameters()
        # Median Blur
        if self.rf_dropdown.currentText() == 'Median':
            # size
            self.rf_layout = QHBoxLayout()
            self.rf_layout.setAlignment(Qt.AlignLeft)
            self.rf_param_size_lbl = QLabel("Size: ")
            self.rf_param_size = QComboBox()
            self.rf_param_size.addItems(filters[filter].getParametersDef()['Size'])
            self.rf_param_size.setCurrentText(params['Size'])
            # pushbutton to set parameters
            self.rf_param_button = QPushButton("set parameters")
            self.rf_param_button.clicked.connect(self.send_filter_params)
            # position and add parameters
            self.rf_layout.addWidget(self.rf_param_size_lbl)
            self.rf_layout.addWidget(self.rf_param_size)
            self.rf_expansion.addLayout(self.rf_layout)
            self.rf_expansion.addWidget(self.rf_param_button)
            # write widgets into list, to keep track
            self.rf_widgets.append(self.rf_param_size_lbl)
            self.rf_widgets.append(self.rf_param_size)
            self.rf_widgets.append(self.rf_param_button)
        elif self.rf_dropdown.currentText() == 'Gaussian':
            # window size
            self.rf_layout = QHBoxLayout()
            self.rf_layout.setAlignment(Qt.AlignLeft)
            self.rf_param_size_lbl = QLabel("Window size: ")
            self.rf_param_size = QComboBox()
            self.rf_param_size.addItems(["choose size", "1x1", "3x3", "5x5", "7x7", "9x9", "11x11"])
            # pushbutton to set parameters
            self.rf_param_button = QPushButton("set parameters")
            # add parameters
            self.rf_layout.addWidget(self.rf_param_size_lbl)
            self.rf_layout.addWidget(self.rf_param_size)
            self.rf_expansion.addLayout(self.rf_layout)
            self.rf_expansion.addWidget(self.rf_param_button)
            # write widgets into list, to keep track
            self.rf_widgets.append(self.rf_param_size_lbl)
            self.rf_widgets.append(self.rf_param_size)
            self.rf_widgets.append(self.rf_param_button)
        # Feedback Filter
        elif self.rf_dropdown.currentText() == 'Feedback':
            # window size
            self.rf_layout = QHBoxLayout()
            self.rf_layout.setAlignment(Qt.AlignLeft)
            self.rf_param_size_lbl = QLabel("Strength in %: ")
            self.rf_param_size = QLineEdit()
            self.rf_param_size.setPlaceholderText("strength e.g. 67%")
            # pushbutton to set parameters
            self.rf_param_button = QPushButton("set parameters")
            # add parameters
            self.rf_layout.addWidget(self.rf_param_size_lbl)
            self.rf_layout.addWidget(self.rf_param_size)
            self.rf_expansion.addLayout(self.rf_layout)
            self.rf_expansion.addWidget(self.rf_param_button)
            # write widgets into list, to keep track
            self.rf_widgets.append(self.rf_param_size_lbl)
            self.rf_widgets.append(self.rf_param_size)
            self.rf_widgets.append(self.rf_param_button)
        else:
            self.no_filter = QLabel("Currently there is no filter selected.")
            self.rf_expansion.addWidget(self.no_filter)
            self.rf_widgets.append(self.no_filter)
        '''

    def of_params(self):
        for i in reversed(range(len(self.of_widgets))):
            self.of_widgets[i].setParent(None)
            del self.of_widgets[-1]

        opt_flow_paramsDef = backend.optical_flow.getParametersDef()
        opt_flow_params = backend.optical_flow.getParameters()

        if self.of_dropdown.currentIndex() == 1:
            # window size
            self.of_layout_w = QHBoxLayout()
            self.of_layout_w.setAlignment(Qt.AlignLeft)
            self.of_win_lbl = QLabel("Window size:")
            self.of_window = QComboBox()
            self.of_window.addItems(opt_flow_paramsDef['winSize'].keys())
            self.of_window.setCurrentText(opt_flow_params['winSize'])
            self.of_layout_w.addWidget(self.of_win_lbl)
            self.of_layout_w.addWidget(self.of_window)
            self.of_expansion.addLayout(self.of_layout_w)
            # max level
            self.of_layout_m = QHBoxLayout()
            self.of_layout_m.setAlignment(Qt.AlignLeft)
            self.of_max_lbl = QLabel("Max. level:")
            self.of_max_lvl = QComboBox()
            self.of_max_lvl.addItems(opt_flow_paramsDef['maxLevel'].keys())
            self.of_max_lvl.setCurrentText(opt_flow_params['maxLevel'])
            self.of_layout_m.addWidget(self.of_max_lbl)
            self.of_layout_m.addWidget(self.of_max_lvl)
            self.of_expansion.addLayout(self.of_layout_m)
            # epsilon
            self.of_layout_e = QHBoxLayout()
            self.of_layout_e.setAlignment(Qt.AlignLeft)
            self.of_epsilon_lbl = QLabel("Epsilon:")
            self.of_epsilon = QLineEdit()
            self.of_epsilon.setText(str(opt_flow_params['epsilon']))
            self.of_layout_e.addWidget(self.of_epsilon_lbl)
            self.of_layout_e.addWidget(self.of_epsilon)
            self.of_expansion.addLayout(self.of_layout_e)
            # count
            self.of_layout_c = QHBoxLayout()
            self.of_layout_c.setAlignment(Qt.AlignLeft)
            self.of_count_lbl = QLabel("Count:")
            self.of_count = QLineEdit()
            self.of_count.setText(str(opt_flow_params['count']))
            self.of_layout_c.addWidget(self.of_count_lbl)
            self.of_layout_c.addWidget(self.of_count)
            self.of_expansion.addLayout(self.of_layout_c)
            # grid-size
            self.of_layout_g = QHBoxLayout()
            self.of_layout_g.setAlignment(Qt.AlignLeft)
            self.of_akku_lbl = QLabel("Accumulation:")
            self.of_akku = QLineEdit()
            self.of_akku.setText(str(opt_flow_params['accumulation']))
            self.of_layout_g.addWidget(self.of_akku_lbl)
            self.of_layout_g.addWidget(self.of_akku)
            self.of_expansion.addLayout(self.of_layout_g)
            # set parameters button
            self.of_param_button = QPushButton("set parameters")
            self.of_param_button.clicked.connect(self.send_opt_flow_params)
            self.of_expansion.addWidget(self.of_param_button)
            # write widgets into list, to keep track
            self.of_widgets.append(self.of_window)
            self.of_widgets.append(self.of_win_lbl)
            self.of_widgets.append(self.of_max_lvl)
            self.of_widgets.append(self.of_max_lbl)
            self.of_widgets.append(self.of_epsilon_lbl)
            self.of_widgets.append(self.of_epsilon)
            self.of_widgets.append(self.of_count_lbl)
            self.of_widgets.append(self.of_count)
            self.of_widgets.append(self.of_akku_lbl)
            self.of_widgets.append(self.of_akku)
            self.of_widgets.append(self.of_param_button)
        else:
            self.no_filter = QLabel("there is currently no filter selected.")
            self.of_expansion.addWidget(self.no_filter)
            self.of_widgets.append(self.no_filter)
        # hs algorithm
        """elif self.of_dropdown.currentIndex() == 2:
            # window size
            self.of_layout_w = QHBoxLayout()
            self.of_layout_w.setAlignment(Qt.AlignLeft)
            self.of_win_lbl = QLabel("Window size:")
            self.of_window = QComboBox()
            self.of_window.addItems(["choose window size", "1x1", "3x3", "5x5", "7x7", "9x9", "11x11", "13x13", "15x15"])
            self.of_layout_w.addWidget(self.of_win_lbl)
            self.of_layout_w.addWidget(self.of_window)
            self.of_expansion.addLayout(self.of_layout_w)
            # max level
            self.of_layout_m = QHBoxLayout()
            self.of_layout_m.setAlignment(Qt.AlignLeft)
            self.of_max_lbl = QLabel("Max. level:")
            self.of_max_lvl = QComboBox()
            self.of_max_lvl.addItems(["choose max level", "1", "2", "3"])
            self.of_layout_m.addWidget(self.of_max_lbl)
            self.of_layout_m.addWidget(self.of_max_lvl)
            self.of_expansion.addLayout(self.of_layout_m)
            # epsilon
            self.of_layout_e = QHBoxLayout()
            self.of_layout_e.setAlignment(Qt.AlignLeft)
            self.of_epsilon_lbl = QLabel("Epsilon:")
            self.of_epsilon = QLineEdit()
            self.of_epsilon.setPlaceholderText("epsilon")
            self.of_layout_e.addWidget(self.of_epsilon_lbl)
            self.of_layout_e.addWidget(self.of_epsilon)
            self.of_expansion.addLayout(self.of_layout_e)
            # count
            self.of_layout_c = QHBoxLayout()
            self.of_layout_c.setAlignment(Qt.AlignLeft)
            self.of_count_lbl = QLabel("Count:")
            self.of_count = QLineEdit()
            self.of_count.setPlaceholderText("count")
            self.of_layout_c.addWidget(self.of_count_lbl)
            self.of_layout_c.addWidget(self.of_count)
            self.of_expansion.addLayout(self.of_layout_c)
            # grid-size
            self.of_layout_g = QHBoxLayout()
            self.of_layout_g.setAlignment(Qt.AlignLeft)
            self.of_grid_lbl = QLabel("Grid size:")
            self.of_grid = QComboBox()
            self.of_grid.addItems(["choose grid size", "1x1", "2x2", "4x4", "8x8"])
            self.of_layout_g.addWidget(self.of_grid_lbl)
            self.of_layout_g.addWidget(self.of_grid)
            self.of_expansion.addLayout(self.of_layout_g)
            # set parameters button
            self.of_param_button = QPushButton("set parameters")
            self.of_expansion.addWidget(self.of_param_button)
            # write widgets into list, to keep track
            self.of_widgets.append(self.of_window)
            self.of_widgets.append(self.of_win_lbl)
            self.of_widgets.append(self.of_max_lvl)
            self.of_widgets.append(self.of_max_lbl)
            self.of_widgets.append(self.of_epsilon_lbl)
            self.of_widgets.append(self.of_epsilon)
            self.of_widgets.append(self.of_count_lbl)
            self.of_widgets.append(self.of_count)
            self.of_widgets.append(self.of_grid_lbl)
            self.of_widgets.append(self.of_grid)
            self.of_widgets.append(self.of_param_button)
        #farnebeck
        elif self.of_dropdown.currentIndex() == 3:
            # window size
            self.of_layout_w = QHBoxLayout()
            self.of_layout_w.setAlignment(Qt.AlignLeft)
            self.of_window_lbl = QLabel("Window size: ")
            self.of_window = QComboBox()
            self.of_window.addItems(["choose window size", "1x1", "3x3", "5x5", "7x7", "9x9", "11x11", "13x13", "15x15"])
            self.of_layout_w.addWidget(self.of_window_lbl)
            self.of_layout_w.addWidget(self.of_window)
            self.of_expansion.addLayout(self.of_layout_w)
            # max level
            self.of_layout_m = QHBoxLayout()
            self.of_layout_m.setAlignment(Qt.AlignLeft)
            self.of_max_lbl = QLabel("Max level: ")
            self.of_max_lvl = QComboBox()
            self.of_max_lvl.addItems(["1", "2", "3"])
            self.of_layout_m.addWidget(self.of_max_lbl)
            self.of_layout_m.addWidget(self.of_max_lvl)
            self.of_expansion.addLayout(self.of_layout_m)
            # pyre scale
            self.of_layout_p = QHBoxLayout()
            self.of_layout_p.setAlignment(Qt.AlignLeft)
            self.of_pyre_lbl = QLabel("Pyre scale: ")
            self.of_pyre = QLineEdit()
            self.of_pyre.setPlaceholderText("pyre-scale")
            self.of_layout_p.addWidget(self.of_pyre_lbl)
            self.of_layout_p.addWidget(self.of_pyre)
            self.of_expansion.addLayout(self.of_layout_p)
            # iteartions
            self.of_layout_i = QHBoxLayout()
            self.of_layout_i.setAlignment(Qt.AlignLeft)
            self.of_iterations_lbl = QLabel("Number of iterations: ")
            self.of_iterations = QLineEdit()
            self.of_iterations.setPlaceholderText("iterations")
            self.of_layout_i.addWidget(self.of_iterations_lbl)
            self.of_layout_i.addWidget(self.of_iterations)
            self.of_expansion.addLayout(self.of_layout_i)
            # poly n
            self.of_layout_pn = QHBoxLayout()
            self.of_layout_pn.setAlignment(Qt.AlignLeft)
            self.of_polyn_lbl = QLabel("Poly n: ")
            self.of_polyn = QLineEdit()
            self.of_polyn.setPlaceholderText("poly n")
            self.of_layout_pn.addWidget(self.of_polyn_lbl)
            self.of_layout_pn.addWidget(self.of_polyn)
            self.of_expansion.addLayout(self.of_layout_pn)
            # poly sigma
            self.of_layout_ps = QHBoxLayout()
            self.of_layout_ps.setAlignment(Qt.AlignLeft)
            self.of_polysigma_lbl = QLabel("Poly sigma: ")
            self.of_polysigma = QLineEdit()
            self.of_polysigma.setPlaceholderText("poly sigma")
            self.of_layout_ps.addWidget(self.of_polysigma_lbl)
            self.of_layout_ps.addWidget(self.of_polysigma)
            self.of_expansion.addLayout(self.of_layout_ps)
            # flex
            self.of_layout_f = QHBoxLayout()
            self.of_layout_f.setAlignment(Qt.AlignLeft)
            self.of_flex_lbl = QLabel("Flex: ")
            self.of_flex = QLineEdit()
            self.of_flex.setPlaceholderText("flex")
            self.of_layout_f.addWidget(self.of_flex_lbl)
            self.of_layout_f.addWidget(self.of_flex)
            self.of_expansion.addLayout(self.of_layout_f)
            # set parameters button
            self.of_param_button = QPushButton("set parameters")
            self.of_expansion.addWidget(self.of_param_button)
            # write widgets into list, to keep track
            self.of_widgets.append(self.of_window_lbl)
            self.of_widgets.append(self.of_window)
            self.of_widgets.append(self.of_max_lbl)
            self.of_widgets.append(self.of_max_lvl)
            self.of_widgets.append(self.of_pyre_lbl)
            self.of_widgets.append(self.of_pyre)
            self.of_widgets.append(self.of_iterations_lbl)
            self.of_widgets.append(self.of_iterations)
            self.of_widgets.append(self.of_polyn_lbl)
            self.of_widgets.append(self.of_polyn)
            self.of_widgets.append(self.of_polysigma_lbl)
            self.of_widgets.append(self.of_polysigma)
            self.of_widgets.append(self.of_flex_lbl)
            self.of_widgets.append(self.of_flex)
            self.of_widgets.append(self.of_param_button)
        """

    # filter on Optical flow
    def of_f_params(self):
        # delete all widgets, else they stack up whenever we change the dropdown
        for i in reversed(range(len(self.of_f_widgets))):
            self.of_f_widgets[i].setParent(None)
            del self.of_f_widgets[-1]
        filters = backend.getFilters()
        filter = backend.getFilter()
        if self.of_f_dropdown.currentText() == 'Gaussian':
            # window size
            self.of_f_layout = QHBoxLayout()
            self.of_f_layout.setAlignment(Qt.AlignLeft)
            self.of_f_param_size_lbl = QLabel("Window size: ")
            self.of_f_param_size = QComboBox()
            self.of_f_param_size.addItems(filters[filter].getParametersDef()['Size'])
            # pushbutton to set parameters
            self.of_f_param_button = QPushButton("set parameters")
            self.of_f_param_button.clicked.connect(self.send_filter_params)
            # add parameters
            self.of_f_layout.addWidget(self.of_f_param_size_lbl)
            self.of_f_layout.addWidget(self.of_f_param_size)
            self.of_f_expansion.addLayout(self.of_f_layout)
            self.of_f_expansion.addWidget(self.of_f_param_button)
            # write widgets into list, to keep track
            self.of_f_widgets.append(self.of_f_param_size_lbl)
            self.of_f_widgets.append(self.of_f_param_size)
            self.of_f_widgets.append(self.of_f_param_button)
        else:
            self.of_f_filter = QLabel("Currently there is no filter selected.")
            self.of_f_expansion.addWidget(self.of_f_filter)
            self.of_f_widgets.append(self.of_f_filter)

    def createSettingsMenu(self):
        self.settingsMenu = QVBoxLayout()
        self.settingsMenu.setSpacing(0)
        self.settingsMenu.setAlignment(Qt.AlignTop)

        # calibrate
        self.calibrate_ROW = QHBoxLayout()
        self.calibrate_ROW.setContentsMargins(5, 10, 5, 0)
        self.calibrate_label = QLabel('Calibrate:    ')
        self.calibrateX_label = QLabel('X: ')
        self.calibrateY_label = QLabel('   Y: ')
        self.calibrateX_edit = QLineEdit()
        self.calibrateX_edit.setText('1.0')
        self.calibrateY_edit = QLineEdit()
        self.calibrateY_edit.setText('1.0')
        self.calibrate_button = QPushButton("set factors")
        self.calibrate_button.clicked.connect(self.send_calibrate)
        self.calibrate_ROW.addWidget(self.calibrate_label)
        self.calibrate_ROW.addWidget(self.calibrateX_label)
        self.calibrate_ROW.addWidget(self.calibrateX_edit)
        self.calibrate_ROW.addWidget(self.calibrateY_label)
        self.calibrate_ROW.addWidget(self.calibrateY_edit)
        self.calibrate_ROW.addWidget(self.calibrate_button)
        self.settingsMenu.addLayout(self.calibrate_ROW)

        # rs: ROI SIZE
        self.rs_ROW = QHBoxLayout()
        self.rs_ROW.setContentsMargins(5, 10, 5, 0)
        self.rs_label = QLabel('ROI data: ')
        self.rs_label.setFixedHeight(15)
        self.rs_ROW.addWidget(self.rs_label)
        self.settingsMenu.addLayout(self.rs_ROW)
        self.settingsMenu.addLayout(self.settings_createRoiSize())

        # rp: Roi Position
        self.settingsMenu.addLayout(self.settings_createRoiPos())
        self.setRoiSize = QPushButton("Draw ROI")

        def click_set():
            if not backend.isDataLoaded():
                return

            if not self.label_original.drawAllowed:
                self.label_original.allowDraw(True)
                self.setRoiSize.setText("Drawing ROI ...")
            else:
                self.label_original.allowDraw(False)
                self.setRoiSize.setText("Draw ROI")

        self.setRoiSize.clicked.connect(click_set)
        self.settingsMenu.addWidget(self.setRoiSize)

        # rohdatenfilter not used
        """ 
        # rf:  ROHDATEN BEARBEITUNG (FILTER)
        self.rf_expansion = ExpansionPanel(title="Apply Filter on raw data")
        # create DropDown
        self.rf_dropdown = QComboBox()
        self.rf_dropdown.addItems(backend.getFilters().keys())
        self.rf_dropdown.setCurrentIndex(0)
        self.rf_expansion.addWidget(self.rf_dropdown)
        # add in the changing parameteres
        self.rf_widgets = []  # make a list so we can store the chaning parameter fields in the expansion panel
        self.settingsMenu.addWidget(self.rf_expansion)
        self.rf_params()
        self.rf_dropdown.currentTextChanged.connect(self.rf_params)
        """

        # of: Verschiebungsberechnung
        self.of_expansion = ExpansionPanel(title="Calculate optical flow")
        self.of_expansion.toggleCollapsed()
        self.of_dropdown = QComboBox()
        self.of_dropdown.addItems(["choose OF algorithm", "Lukas Kanade"])  # , "Horn–Schunck", "Farneback Algorithm"])
        self.of_dropdown.setCurrentIndex(1)
        self.of_expansion.addWidget(self.of_dropdown)
        self.of_widgets = []
        self.settingsMenu.addWidget(self.of_expansion)
        self.of_params()
        self.of_dropdown.currentTextChanged.connect(self.of_params)

        # Filter auf Verschiebung
        self.of_f_expansion = ExpansionPanel(title="Apply filters on calculated optical flow")
        # create DropDown
        self.of_f_dropdown = QComboBox()
        self.of_f_dropdown.addItems(backend.getFilters().keys())
        self.of_f_dropdown.setCurrentIndex(0)
        self.of_f_expansion.addWidget(self.of_f_dropdown)
        # add in the changing parameteres
        self.of_f_widgets = []  # make a list so we can store the chaning parameter fields in the expansion panel
        self.settingsMenu.addWidget(self.of_f_expansion)
        self.of_f_params()
        self.of_f_dropdown.currentTextChanged.connect(self.of_f_params)

        # Dehnungsberechnung
        self.strain_expansion = ExpansionPanel(title="Calculate strain")
        self.strain_ROW = QHBoxLayout()
        self.strain_dX_lbl = QLabel("dX: ")
        self.strain_dX_lbl.setToolTip("TODO Tooltip -> Andreas")
        self.strain_dX = QLineEdit()
        self.strain_dX.setText("ToDo connect to BE")
        self.strain_dX.setToolTip("TODO Tooltip -> Andreas")
        self.strain_dY_lbl = QLabel("dY: ")
        self.strain_dY = QLineEdit()
        self.strain_dY.setText("ToDo connect to BE")
        self.strain_ROW.addWidget(self.strain_dX_lbl)
        self.strain_ROW.addWidget(self.strain_dX)
        self.strain_ROW.addWidget(self.strain_dY_lbl)
        self.strain_ROW.addWidget(self.strain_dY)
        self.strain_expansion.addLayout(self.strain_ROW)
        self.settingsMenu.addWidget(self.strain_expansion)

        # Punkt für historical Graph einstellen können
        self.tr_label = QLabel('Historical graph point: ')
        self.tr_label.setFixedHeight(15)
        self.settingsMenu.addWidget(self.tr_label)
        self.settingsMenu.addLayout(self.settings_createGraphPoint())

        # Wrap in Scroll Area
        self.settingsMenuScrollArea = QScrollArea()
        self.settingsMenuScrollArea.setStyleSheet("#SettingsScrollview {background: white; border: 0; margin: 0}")
        self.settingsMenuWrap = QWidget()
        self.settingsMenuScrollArea.setObjectName("SettingsScrollview")
        self.settingsMenuWrap.setObjectName("SettingsScrollview")
        self.settingsMenuWrap.setLayout(self.settingsMenu)

        self.settingsMenuScrollArea.setWidget(self.settingsMenuWrap)
        self.settingsMenuScrollArea.setWidgetResizable(True)
        return self.settingsMenuScrollArea

    def settings_createRoiSize(self):
        self.rs_ROW = QHBoxLayout()
        self.rs_ROW.setContentsMargins(5, 0, 5, 0)
        self.rs_x_edit = QLineEdit(self)
        self.rs_x_edit.textChanged.connect(lambda w: self.updateRoi(None, None, w, None))
        self.rs_y_edit = QLineEdit(self)
        self.rs_y_edit.textChanged.connect(lambda h: self.updateRoi(None, None, None, h))
        self.w_label = QLabel('Width: ')
        self.w_label.setFixedWidth(45)

        self.h_label = QLabel(' Height: ')
        self.h_label.setFixedWidth(48)
        # add everything to the GUI Row2
        self.rs_ROW.addWidget(self.w_label)
        self.rs_ROW.addWidget(self.rs_x_edit)
        self.rs_ROW.addWidget(self.h_label)
        self.rs_ROW.addWidget(self.rs_y_edit)
        self.rs_ROW.addWidget(getHelpIcon("Set the window size of the ROI here"))
        return self.rs_ROW

    def updateRoi(self, x, y, width, height):
        if not backend.isDataLoaded() or not self.updateRoiAllowed:
            return
        self.setRoiSize.setText("Draw ROI")
        nx0 = int(x) if x else int(self.rp_x_edit.text())
        ny0 = int(y) if y else int(self.rp_y_edit.text())
        nx1 = nx0 + (int(width) if width else int(self.rs_x_edit.text()))
        ny1 = ny0 + (int(height) if height else int(self.rs_y_edit.text()))
        self.label_original.setRectangle(int(nx0), int(ny0), int(nx1), int(ny1))
        self.roi = [int(nx0), int(ny0), int(nx1), int(ny1)]
        self.updateWindow()

    def updateHist(self, x, y):
        if not backend.isDataLoaded() or not self.updateHistAllowed:
            return
        self.setPoint.setText("Set Point")
        nx = int(x) if x else int(self.p_x_edit.text())
        ny = int(y) if y else int(self.p_y_edit.text())
        self.label_original.setHistPoint(int(nx), int(ny))

    def settings_createRoiPos(self):
        self.rp_ROW = QHBoxLayout()
        self.rp_ROW.setContentsMargins(5, 0, 5, 10)

        self.rp_x_edit = QLineEdit(self)
        self.rp_x_edit.textChanged.connect(lambda x: self.updateRoi(x, None, None, None))
        self.rp_y_edit = QLineEdit(self)
        self.rp_y_edit.textChanged.connect(lambda y: self.updateRoi(None, y, None, None))
        self.x_label = QLabel('x: ')
        self.x_label.setFixedWidth(45)
        self.y_label = QLabel(' y: ')
        self.y_label.setFixedWidth(48)
        self.rp_ROW.addWidget(self.x_label)
        self.rp_ROW.addWidget(self.rp_x_edit)
        self.rp_ROW.addWidget(self.y_label)
        self.rp_ROW.addWidget(self.rp_y_edit)

        self.rp_ROW.addWidget(getHelpIcon("ROI position is shown for lower left edge of ROI window"))
        return self.rp_ROW

    def settings_createGraphPoint(self):
        self.p_ROW = QHBoxLayout()
        self.p_ROW.setContentsMargins(5, 0, 5, 10)

        self.p_x_edit = QLineEdit(self)
        self.p_x_edit.setPlaceholderText("e.g. 12")
        self.p_x_edit.textChanged.connect(lambda x: self.updateHist(x, None))
        self.p_y_edit = QLineEdit(self)
        self.p_y_edit.setPlaceholderText("e.g. 45")
        self.p_y_edit.textChanged.connect(lambda y: self.updateHist(None, y))
        self.p_x_label = QLabel('       x: ')
        self.p_y_label = QLabel('         y: ')
        self.p_ROW.addWidget(self.p_x_label)
        self.p_ROW.addWidget(self.p_x_edit)
        self.p_ROW.addWidget(self.p_y_label)
        self.p_ROW.addWidget(self.p_y_edit)

        self.p_ROW.addWidget(getHelpIcon("ROI position is shown for lower left edge of ROI window"))
        self.setPoint = QPushButton("set Point")

        def clickedHist():
            if not backend.isDataLoaded():
                return
            if self.label_original.draw_hist:
                self.label_original.allowHist(False)
                self.setPoint.setText("Set Point")
            else:
                self.label_original.allowHist(True)
                self.setPoint.setText("Setting Point ...")

        self.setPoint.clicked.connect(clickedHist)
        self.setPoint.setFixedWidth(80)
        self.p_ROW.addWidget(self.setPoint)
        return self.p_ROW

    def createVideoMenu(self):
        self.videoMenu = QVBoxLayout()
        self.videoMenu.setSpacing(0)
        self.videoMenu.setAlignment(Qt.AlignTop)

        def setTimeRange(e):
            if self.frame > e[1]:
                self.frame = e[1]
                self.setVideoPause()
                self.updateWindow()
            if self.frame < e[0]:
                self.frame = e[0]
                self.setVideoPause()
                self.updateWindow()

        self.tr_slider = QLabeledRangeSlider(Qt.Horizontal)
        self.tr_slider.setMinimum(0)
        self.tr_slider.setMaximum(1)
        self.tr_slider.setValue((0, 0))

        self.tr_slider.valueChanged.connect(lambda e: setTimeRange(e))
        self.videoMenu.addWidget(self.tr_slider)

        self.video1 = ExpansionPanel(title="Raw data")
        self.video1.toggleCollapsed()
        self.label_original = DrawLabel(self)

        def setRoi(e):
            # e : (x0,y0,x1,y1)
            self.updateRoiAllowed = False
            self.rp_x_edit.setText(str(e[0]))
            self.rp_y_edit.setText(str(e[1]))
            self.rs_x_edit.setText(str(e[2] - e[0]))
            self.rs_y_edit.setText(str(e[3] - e[1]))
            self.setRoiSize.setText("Draw ROI")
            self.updateRoiAllowed = True

            self.roi = [e[0], e[1], e[2], e[3]]
            self.updateWindow()

        self.label_original.rectangleChanged.connect(lambda e: setRoi(e))

        def setHist(histPoint):
            # histPoint : (x,y)
            self.updateHistAllowed = False
            self.p_x_edit.setText(str(histPoint[0]))
            self.p_y_edit.setText(str(histPoint[1]))
            self.setPoint.setText("Set Point")
            self.updateHistAllowed = True

        self.label_original.histChanged.connect(lambda e: setHist(e))

        pixmap = QPixmap(':gui_resources/videoicon.png').scaled(100, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.label_original.setAlignment(Qt.AlignCenter)
        self.label_original.setPixmap(pixmap)
        self.video1.addWidget(self.label_original)
        self.videoMenu.addWidget(self.video1)

        # raw data filter on video view - not used
        """
        self.video2 = ExpansionPanel(title="Raw data with applied filter")
        #: "+ self.rf_dropdown.currentText()+ "{'Windowsize':'" + str(self.rf_param_size.currentText()) + "'}")
        self.label_filtered = QLabel(self)
        pixmap = QPixmap(':gui_resources/videoicon.png').scaled(100, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.label_filtered.setAlignment(Qt.AlignCenter)
        self.label_filtered.setPixmap(pixmap)
        self.video2.addWidget(self.label_filtered)
        self.videoMenu.addWidget(self.video2)
        """

        self.video3 = ExpansionPanel(title="Optical Flow")
        self.video3.toggleCollapsed()
        self.label_flow = QLabel(self)
        pixmap = QPixmap(':gui_resources/videoicon.png').scaled(100, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.label_flow.setAlignment(Qt.AlignCenter)
        self.label_flow.setPixmap(pixmap)

        ofLayout = QHBoxLayout()
        ColorBar = CustomColorBar()
        ColorBar.changeColorBar("", "")
        ColorBar.setFixedWidth(25)

        ofLayout.addWidget(self.label_flow)
        ofLayout.addWidget(ColorBar)
        self.video3.addLayout(ofLayout)
        self.videoMenu.addWidget(self.video3)

        self.video4 = ExpansionPanel(title="Optical flow with filter")
        self.label_preview = QLabel(self)
        pixmap = QPixmap(':gui_resources/videoicon.png').scaled(100, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.label_preview.setAlignment(Qt.AlignCenter)
        self.label_preview.setPixmap(pixmap)
        self.video4.addWidget(self.label_preview)
        self.videoMenu.addWidget(self.video4)

        self.video5 = ExpansionPanel(title="Strain")
        self.video5.toggleCollapsed()
        self.label_prev = QLabel(self)
        pixmap = QPixmap(':gui_resources/videoicon.png').scaled(100, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.label_prev.setAlignment(Qt.AlignCenter)
        self.label_prev.setPixmap(pixmap)

        strainLayout = QHBoxLayout()
        ColorBar = CustomColorBar()
        ColorBar.changeColorBar("", "")
        ColorBar.setFixedWidth(25)

        strainLayout.addWidget(self.label_prev)
        strainLayout.addWidget(ColorBar)

        self.video5.addLayout(strainLayout)
        self.videoMenu.addWidget(self.video5)

        # PLOT for the historical graph
        self.plot_panel = ExpansionPanel(title="Historical graph")
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)
        self.toolbarWrapper = QHBoxLayout()
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.setFixedHeight(24)
        self.toolbarWrapper.addWidget(self.toolbar)
        self.plot_exp_button = QPushButton('Export data as table')
        self.toolbarWrapper.addWidget(self.plot_exp_button)

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        plotWrap = QWidget()
        plotWrap.setMinimumHeight(300)
        plotWrap.setObjectName("ScrollviewWidget")
        plotWrap.setLayout(layout)
        self.plot_panel.addLayout(self.toolbarWrapper)
        self.plot_panel.addWidget(plotWrap)
        self.videoMenu.addWidget(self.plot_panel)
        self.plot()

        self.scrollVideosMenu = QScrollArea()
        self.scrollVideosMenu.setObjectName("Scrollview")
        self.videosMenuWrap = QWidget()
        self.videosMenuWrap.setObjectName("ScrollviewWidget")
        self.videosMenuWrap.setLayout(self.videoMenu)

        self.scrollVideosMenu.setWidget(self.videosMenuWrap)
        self.scrollVideosMenu.setWidgetResizable(True)
        return self.scrollVideosMenu

    def createTopMenuBar(self):
        # Top Menu Bar
        self.topLayout = QHBoxLayout()

        label = QLabel(self)
        pixmap = QPixmap(':gui_resources/BAM.png').scaled(100, 40, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        label.setPixmap(pixmap)
        self.resize(pixmap.width(), pixmap.height())
        self.topLayout.addWidget(label)
        self.menuBar = QVBoxLayout()
        self.menuBar_horizontal = QHBoxLayout()

        # import data
        self.projectLocation = QLineEdit(None, self)
        self.projectLocation.setReadOnly(True)
        self.projectLocation.setPlaceholderText("after importing data, the path will be visible here")
        self.menuBar_horizontal.addWidget(self.projectLocation)
        self.menuBar_horizontal.addWidget(getHelpIcon("Open old project or import new data as .TIF"))
        self.importProject = QPushButton("Import", self)
        self.importProject.setFixedWidth(80)
        self.menuBar_horizontal.addWidget(self.importProject)
        self.menuBar.addLayout(self.menuBar_horizontal)
        # File Dialog für den Import
        self.importProject.clicked.connect(self.filedialogue)

        # Menu Bar bottom
        self.menuBarBottom = QHBoxLayout()
        self.normalButtonMenus = QHBoxLayout()
        self.normalButtonMenus.setAlignment(Qt.AlignLeft)

        def changeName():
            dlg = QInputDialog(self)
            dlg.setInputMode(QInputDialog.TextInput)
            dlg.setWindowTitle('Change Project Title')
            dlg.setLabelText("New Title:")
            dlg.resize(220, 100)
            ok = dlg.exec_()
            if ok:
                self.setTitle(dlg.textValue())

        self.fileButton = QPushButton("Change Project Title")
        self.fileButton.setFixedWidth(145)
        self.fileButton.clicked.connect(changeName)

        self.normalButtonMenus.addWidget(self.fileButton)
        self.savebutton = QPushButton("Save")
        self.savebutton.setFixedWidth(100)
        self.savebutton.clicked.connect(self.toJSON)
        self.normalButtonMenus.addWidget(self.savebutton)

        def openExport():
            dlg = QInputDialog(self)
            dlg.setWindowTitle("Export")
            dlg.setLabelText("Choose Export Variant")
            dlg.setComboBoxItems(["Export as one", "Export seperately"])
            dlg.resize(220, 100)
            ok = dlg.exec_()
            if ok:
                if dlg.textValue() == "Export as one":
                    self.exportProject(True)
                else:
                    self.exportProject(False)

        self.exportButton = QPushButton("Export")
        self.exportButton.setFixedWidth(100)
        self.exportButton.clicked.connect(openExport)
        self.normalButtonMenus.addWidget(self.exportButton)
        self.helpButton = QPushButton("Help")
        self.helpButton.setFixedWidth(100)
        self.normalButtonMenus.addWidget(self.helpButton)
        self.helpButton.clicked.connect(self.openGIT)
        self.menuBarBottom.addLayout(self.normalButtonMenus)

        # add the video player buttons
        self.play_button = QPushButton()
        self.play_button.setIcon(getIconInColor(":gui_resources/play.png", "green"))
        setIconButtonStyle(self.play_button, "Play/Pause")
        self.play_button.clicked.connect(
            lambda: self.playvideo(VideoButtons.PAUSE if self.playing == VideoButtons.PLAY else VideoButtons.PLAY))

        self.stop_button = QPushButton()
        self.stop_button.setIcon(getIconInColor(":gui_resources/stop.png", "red"))
        setIconButtonStyle(self.stop_button, "Stop")
        self.stop_button.clicked.connect(lambda: self.playvideo(VideoButtons.STOP))

        self.bw_button = QPushButton()
        self.bw_button.setIcon(QIcon(":gui_resources/fast-forward.png"))
        setIconButtonStyle(self.bw_button, "change Directions")
        self.bw_button.clicked.connect(self.changeDirection)

        self.s_bw_button = QPushButton()
        self.s_bw_button.setIcon(QIcon(":gui_resources/sort-left.png"))
        setIconButtonStyle(self.s_bw_button, "Single Frame Backward")
        self.s_bw_button.clicked.connect(lambda: self.playvideo(VideoButtons.SINGLE_BW))

        self.s_fw_button = QPushButton()
        self.s_fw_button.setIcon(QIcon(":gui_resources/sort-right.png"))
        setIconButtonStyle(self.s_fw_button, "Single Frame Forward")
        self.s_fw_button.clicked.connect(lambda: self.playvideo(VideoButtons.SINGLE_FW))

        playbar = QHBoxLayout()
        playbar.setContentsMargins(0, 0, 10, 0)
        playbar.addWidget(self.play_button)
        playbar.addWidget(self.stop_button)
        playbar.addWidget(self.bw_button)
        playbar.addWidget(self.s_bw_button)
        playbar.addWidget(self.s_fw_button)
        playbar.setAlignment(Qt.AlignHCenter)
        self.menuBarBottom.addLayout(playbar)

        self.menuBar.addLayout(self.menuBarBottom)
        self.topLayout.addLayout(self.menuBar, 10)
        return self.topLayout

    def exportProject(self, asOne):
        if not (backend.isDataLoaded()):
            self.error_dialog = QMessageBox.critical(self, "Export Error", 'There are no videos to export!')
            return
        dir_ = self.toJSON()
        if not dir_:
            return
        dir_name = dir_ + "/" + self.projectname
        time = self.tr_slider.value()
        self.exportDialog = QMessageBox(self)
        self.exportDialog.setText('The Export can take some Time with a lot of Frames (up to 90min)! Continue Exporting?')
        self.exportDialog.setWindowTitle("Export Information")
        self.exportDialog.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

        if (self.exportDialog.exec() == QMessageBox.Ok):
            self.progressDialog2 = QProgressDialog("Exporting...", "Cancel", 0, 100, self)
            self.thread2 = QThread()
            self.worker2 = Exporter()
            self.worker2.dir_name = dir_name
            self.worker2.time = time
            self.worker2.asOne = asOne

            self.worker2.moveToThread(self.thread2)
            self.thread2.started.connect(self.worker2.run)
            self.worker2.finished.connect(self.thread2.quit)

            def cancel2():
                self.worker2.stop()
                self.thread2.quit()

            self.progressDialog2.canceled.connect(cancel2)
            self.worker2.progress.connect(self.progressDialog2.setValue)
            self.worker2.max.connect(self.progressDialog2.setMaximum)
            self.thread2.start()
            self.thread2.finished.connect(lambda: self.progressDialog2.close())
    

    def toJSON(self):
        roi = self.label_original.getRectangle()
        hist = self.label_original.getHistPoint()
        time = self.tr_slider.value()
        dir_ = QFileDialog.getExistingDirectory(None, 'Select a folder:', 'C:\\', QFileDialog.ShowDirsOnly)
        backend.projectJSON(dir_, self.projectname, self.projectLocation.text(), roi, hist, time)
        return dir_

    def setTitle(self, title):
        self.projectname = title
        widget.setWindowTitle(title)

    def openGIT(self):
        QDesktopServices.openUrl(QUrl("https://git.imp.fu-berlin.de/fu1106jv/swp-telematik"))

    def updateTimer(self):
        if not backend.isDataLoaded():
            return

        if self.tr_slider.minimum() != 0:
            self.tr_slider.setMinimum(0)
        if self.tr_slider.maximum() != backend.getImgCount():
            self.tr_slider.setMaximum(backend.getImgCount())

        # self.tr_slider.value() -> (min, max)
        if self.playing == VideoButtons.PAUSE:
            pass
        elif self.playing == VideoButtons.STOP:
            if self.frame != 0:
                self.frame = self.tr_slider.value()[0]
                self.updateWindow()
        elif self.playing == VideoButtons.PLAY:
            self.updateWindow()
            self.frame = self.frame + self.direction
        elif self.playing == VideoButtons.SINGLE_FW:
            self.updateWindow()
            self.frame = self.frame + 1
            self.setVideoPause()
        elif self.playing == VideoButtons.SINGLE_BW:
            self.updateWindow()
            self.frame = self.frame - 1
            self.setVideoPause()

        # Min and Max Frames
        if self.frame > self.tr_slider.value()[1]:
            self.frame = self.tr_slider.value()[1]
            self.setVideoPause()
            self.updateWindow()
        if self.frame < self.tr_slider.value()[0]:
            self.frame = self.tr_slider.value()[0]
            self.setVideoPause()
            self.updateWindow()

    def updateWindow(self):
        print('update')
        im_org, _, _, _, _ = backend.getImageFlow(self.frame, self.of_colorDirection, self.roi)
#        im_org, (img_opt_filter_x,img_opt_filter_y), (img_opt_x,img_opt_y),(img_strain_x,img_strain_y),_ = backend.getImageFlow(self.frame, self.of_colorDirection, self.roi)
#        img_opt = np.vstack((img_opt_x,img_opt_y))
#        img_opt_filter = np.vstack((img_opt_filter_x,img_opt_filter_y))
#        img_strain = np.vstack((img_strain_x,img_strain_y))

        q = QImage(im_org.data, im_org.shape[1], im_org.shape[0], QImage.Format_BGR888)
        pixmap = QPixmap(q)
        self.label_original.setFixedWidth(im_org.shape[1])
        self.label_original.setMaximumHeight(im_org.shape[0])
        self.label_original.setAlignment(Qt.AlignCenter)
        self.label_original.setPixmap(pixmap)


#        q = QImage(img_opt.data, img_opt.shape[1], img_opt.shape[0], QImage.Format_BGR888)
#        pixmap = QPixmap(q)
#        self.label_flow.setFixedWidth(img_opt.shape[1])
#        self.label_flow.setMaximumHeight(img_opt.shape[0])
#        self.label_flow.setAlignment(Qt.AlignCenter)
#        self.label_flow.setPixmap(pixmap)

#        q = QImage(img_opt_filter.data, img_opt_filter.shape[1], img_opt_filter.shape[0], QImage.Format_BGR888)
#        pixmap = QPixmap(q)
#        self.label_preview.setFixedWidth(img_opt_filter.shape[1])
#        self.label_preview.setMaximumHeight(img_opt_filter.shape[0])
#        self.label_preview.setAlignment(Qt.AlignCenter)
#        self.label_preview.setPixmap(pixmap)

#        q = QImage(img_strain.data, img_strain.shape[1], img_strain.shape[0], QImage.Format_BGR888)
#        pixmap = QPixmap(q)
#        self.label_prev.setFixedWidth(img_strain.shape[1])
#        self.label_prev.setMaximumHeight(img_strain.shape[0])
#        self.label_prev.setAlignment(Qt.AlignCenter)
#        self.label_prev.setPixmap(pixmap)

    def playvideo(self, vb):
        if not backend.isDataLoaded():
            return
        self.playing = vb
        if vb == VideoButtons.PLAY:
            self.play_button.setIcon(QIcon(":gui_resources/pause.png"))
        else:
            self.play_button.setIcon(getIconInColor(":gui_resources/play.png", "green"))

    def changeDirection(self):
        self.direction = self.direction * (-1)
        if self.direction == 1:
            self.bw_button.setIcon(QIcon(":gui_resources/fast-forward.png"))
        else:
            self.bw_button.setIcon(QIcon(":gui_resources/rewind.png"))

    def send_opt_flow_params(self):
        backend.optical_flow.setParameters({'winSize': self.of_window.currentText(),
                                            'maxLevel': self.of_max_lvl.currentText(),
                                            'epsilon': self.of_epsilon.text(),
                                            'count': self.of_count.text(),
                                            'accumulation': int(self.of_akku.text())
                                            })
        self.playvideo(VideoButtons.SINGLE_FW)
        self.playvideo(VideoButtons.SINGLE_BW)

    def send_calibrate(self):
        backend.color_scaling_X = float(self.calibrateX_edit.text())
        backend.color_scaling_Y = float(self.calibrateY_edit.text())
        self.playvideo(VideoButtons.SINGLE_FW)
        self.playvideo(VideoButtons.SINGLE_BW)
        
    def send_filter_params(self):
        filters = backend.getFilters()
        filter = backend.getFilter()
        params = {'Size': self.of_f_param_size.currentText()}
        filters[filter].setParameters(params)
        self.playvideo(VideoButtons.SINGLE_FW)
        self.playvideo(VideoButtons.SINGLE_BW)

    """    def change_of_color_direction(self):
            if self.of_colorDirection == "y":
                self.of_colorDirection = "xy"
                self.of_changeColoring.setIcon(QIcon(":gui_resources/up-right-arrow.png"))
                self.of_changeColoring.setToolTip("OF Coloring Direction: x and y")
    
            elif self.of_colorDirection == "xy":
                self.of_colorDirection = "x"
                self.of_changeColoring.setIcon(QIcon(":gui_resources/long-arrow-right.png"))
                self.of_changeColoring.setToolTip("OF Coloring Direction: x")
    
            elif self.of_colorDirection == "x":
                self.of_colorDirection = "y"
                self.of_changeColoring.setIcon(QIcon(":gui_resources/long-arrow-up.png"))
                self.of_changeColoring.setToolTip("OF Coloring Direction: y")
            self.updateWindow()
    
        def change_of_color_direction_strain(self):
            if self.strain_colorDirection == "y":
                self.strain_colorDirection = "xy"
                self.strain_changeColoring.setIcon(QIcon(":gui_resources/up-right-arrow.png"))
                self.strain_changeColoring.setToolTip("OF Coloring Direction: x and y")
    
            elif self.strain_colorDirection == "xy":
                self.strain_colorDirection = "x"
                self.strain_changeColoring.setIcon(QIcon(":gui_resources/long-arrow-right.png"))
                self.strain_changeColoring.setToolTip("OF Coloring Direction: x")
    
            elif self.strain_colorDirection == "x":
                self.strain_colorDirection = "y"
                self.strain_changeColoring.setIcon(QIcon(":gui_resources/long-arrow-up.png"))
                self.strain_changeColoring.setToolTip("OF Coloring Direction: y")
            self.updateWindow()
    """

    def setVideoPause(self):
        self.playing = VideoButtons.PAUSE
        self.play_button.setIcon(getIconInColor(":gui_resources/play.png", "green"))


if __name__ == "__main__":
    app = QApplication([])
    widget = AppWidget()
    widget.resize(1000, 600)
    widget.setObjectName("mainWidget")
    widget.setStyleSheet(styleSheet)
    widget.show()
    widget.projectname = "Project Title"
    widget.setWindowTitle(widget.projectname)
    sys.exit(app.exec_())
