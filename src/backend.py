import os
import cv2
import numpy as np
import datetime
import json
import multiprocessing
from scipy.ndimage.filters import gaussian_filter # , median_filter
# from scipy.signal import butter, filtfilt

#############################################################################################
class FILTER_INTERFACE:
    def __init__(self, name):
        self.name = name
        return

    def getParametersDef(self):
        raise NotImplementedError

    def getParameters(self):
        raise NotImplementedError

    def setParameters(self, params):
        raise NotImplementedError

    def process(self, src):
        raise NotImplementedError


#############################################################################################
class FILTER_MEDIAN(FILTER_INTERFACE):
    def init(self):
        self.sizeDict = {'1': 1,
                         '3': 3,
                         '5': 5,
                         '7': 7,
                         '9': 9,
                         '11': 11,
                         '13': 13,
                         '15': 15,
                         }

        self.size = '1'

    def getParametersDef(self):
        return {'Size': self.sizeDict}

    def getParameters(self):
        return {'Size': self.size}

    def setParameters(self, params):
        self.size = params['Size']
        return

    def process(self, src):
        return cv2.medianBlur(src, self.sizeDict[self.size])


#############################################################################################
class FILTER_GAUSSIAN(FILTER_INTERFACE):
    def init(self):
        self.sizeDict = {'1': 1,
                         '3': 3,
                         '5': 5,
                         '7': 7,
                         '9': 9,
                         '11': 11,
                         '13': 13,
                         '15': 15,
                         }

        self.size = '1'

    def getParametersDef(self):
        return {'Size': self.sizeDict}

    def getParameters(self):
        return {'Size': self.size}

    def setParameters(self, params):
        self.size = params['Size']
        return

    def process(self, src):
        return gaussian_filter(src,sigma=self.sizeDict[self.size])

#############################################################################################
class OPTICAL_FLOW:
    def __init__(self):
        self.winSizeDict = {'3x3': (3, 3),
                            '5x5': (5, 5),
                            '7x7': (7, 7),
                            '9x9': (9, 9),
                            '11x11': (11, 11),
                            '13x13': (13, 13),
                            '15x15': (15, 15),
                            '30x30': (30, 30),
                            '60x60': (60, 60)
                            }

        self.winSize = '15x15'

        self.maxLevelDict = {'0': 0, '1': 1, '2': 2, '3': 3}

        self.count = 10
        self.epsilon = 0.03
        self.accumulation = 10

        self.parametersDef = {'winSize': self.winSizeDict,
                              'maxLevel': self.maxLevelDict,
                              'epsilon': [0.0, 1.0],
                              'count': [0, 100],
                              'accumulation': self.accumulation
                              }

        self.maxLevel = '3'

        self.win = self.winSizeDict[self.winSize]

        self.lk_params = dict(winSize=self.win,
                              maxLevel=self.maxLevelDict[self.maxLevel],
                              criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, self.count, 0.03))

        self.of_pos = None

    #    print('thread',number)
    #    return number


    def func(self,a,b,e):
        print('waiting')
        e.wait()
        print(a,b)
        print('1')
        p, st, err = cv2.calcOpticalFlowPyrLK(self.img_0_, self.img_1_, self.of_pos[a:b], None, **self.lk_params)
        self.q.put(p)
    #    p1_m = p1
        print('2')
        return


    def init(self, width, height):
        global process1

        self.width = width
        self.height = height

        self.opticalFlow_data = None
        self.opticalFlow_idx = 0

#        self.process1_start = multiprocessing.Event()
#        self.process1_start.clear()

#        process1 = multiprocessing.Process(target=self.func, args=(0,200,self.process1_start))#len(self.of_pos)))
#        process1.start()
#        process1.

#        self.process_start = Event()
#        self.process_stop = Event()
#        self.pool = Pool(16)
#        self.a = self.pool.starmap(self.collect_result)#,np.array_split(self.of_pos,16))
#        self.pool.close()
#        self.pool.close()
#        self.pool.join()
#        print(self.a.get())
#            p.join
#        self.pool = Pool(16)
#        self.pool.map(self.collect_result)
#        self.pool.close()
#        self.pool.join()

    def getParametersDef(self):
        return self.parametersDef

    def getParameters(self):
        return {'winSize': self.winSize,
                'maxLevel': self.maxLevel,
                'epsilon': self.epsilon,
                'count': self.count,
                'accumulation': self.accumulation
                }

    def setParameters(self, params):
        print('setparam: ',)
        self.winSize = params['winSize']
        self.lk_params = dict(winSize=self.winSizeDict[self.winSize],
                              maxLevel=self.maxLevelDict[params['maxLevel']],
                              criteria=(cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, int(params['count']),
                                        float(params['epsilon'])))
        self.accumulation = params['accumulation']
        self.init(self.width, self.height)

    def process(self, img_0, img_1):
        p1, st, err = cv2.calcOpticalFlowPyrLK(img_0, img_1, self.of_pos, None, **self.lk_params)
        img_0_ = cv2.cvtColor(img_0, cv2.COLOR_GRAY2BGR)
        mask = np.full(img_0_.shape, 255, dtype=np.uint8)
        p1 = p1 - self.of_pos
        for i in range(len(self.of_pos)):
            a, b = self.of_pos[i][0]
            c, d = np.round(self.of_pos[i][0] + p1[i][0] * 8)
            mask = cv2.line(mask, (int(a), int(b)), (int(c), int(d)), [0, 0, 255], 1)
        return img_0_ & mask

    def collect_result(self):
        return 'test'

    def process_calc(self, p0):
        p1, st, err = cv2.calcOpticalFlowPyrLK(self.img_0, self.img_1, p0, None, **self.lk_params)
        return p1

    def process_a(self):
#        self.new_pool = Pool(16)
        return

    def process_1(self, img_0, img_1,direction):

#        global img_0_,img_1_,process1_start,process1
        self.img_0_ = img_0
        self.img_1_ = img_1

        t_a = datetime.datetime.now()

        self.q = multiprocessing.Queue()


        self.p1_m = np.full(self.of_pos.shape,0)
        number = 50000
#        process1 = multiprocessing.Process(target=self.func, args=(0,200))#len(self.of_pos)))

#        process2 = multiprocessing.Process(target=self.func, args=(200,400))#len(self.of_pos)))
#        process2 = multiprocessing.Process(target=func, args=(number,))

#        process1.start()
#        process2.start()

#        self.process1_start.set()
#        process1.join()
#        process2.join()
        p1 = self.p1_m

        t_b = datetime.datetime.now()
        print('time: ',(t_b - t_a))

        grid = self.gridSizeDict[self.gridSize]
#        p1, st, err = cv2.calcOpticalFlowPyrLK(self.img_0, self.img_1, self.of_pos, None, **self.lk_params)
        img_0_ = cv2.cvtColor(img_0, cv2.COLOR_GRAY2BGR)
        mask = np.full((self.height // grid, self.width // grid, 3), 128, dtype=np.uint8)
        print(mask.shape,img_0_.shape)
        p1 = p1 - self.of_pos
#        print(p1)
        for y in range(0, self.height // grid):
            for x in range(0, self.width // grid):
                p = p1[y * self.width // grid + x]
                if direction.find('x') != -1:
                    mask[y,x,0] = mask[y,x,0] + 127 * np.clip(p[0, 0],0,1.0)
                if direction.find('y') != -1:
                    mask[y,x,1] = mask[y,x,1] + 127 * np.clip(p[0, 1],0,1.0)
                mask[y,x,2] = 0
#        for i in range(len(self.of_pos)):
#            a, b = self.of_pos[i][0]
#            c, d = np.round(self.of_pos[i][0] + p1[i][0] * 8)
#            mask = cv2.line(mask, (int(a), int(b)), (int(c), int(d)), [0, 0, 255], 1)
        return cv2.resize(mask, dsize=(self.width, self.height), interpolation=cv2.INTER_CUBIC)

    def lucas_kanade_np(self, im1, im2, win=2):
        assert im1.shape == im2.shape

        win = int(int(self.winSizeDict[self.winSize][0]) + 1 / 2)

        I_x = np.zeros(im1.shape)
        I_y = np.zeros(im1.shape)
        I_t = np.zeros(im1.shape)
        I_x[1:-1, 1:-1] = (im1[1:-1, 2:] - im1[1:-1, :-2]) / 2
        I_y[1:-1, 1:-1] = (im1[2:, 1:-1] - im1[:-2, 1:-1]) / 2
        I_t[1:-1, 1:-1] = im1[1:-1, 1:-1] - im2[1:-1, 1:-1]
        params = np.zeros(im1.shape + (5,)) #Ix2, Iy2, Ixy, Ixt, Iyt
        params[..., 0] = I_x * I_x # I_x2
        params[..., 1] = I_y * I_y # I_y2
        params[..., 2] = I_x * I_y # I_xy
        params[..., 3] = I_x * I_t # I_xt
        params[..., 4] = I_y * I_t # I_yt
        del I_x, I_y, I_t
        cum_params = np.cumsum(np.cumsum(params, axis=0), axis=1)
        del params
        win_params = (cum_params[2 * win + 1:, 2 * win + 1:] -
                    cum_params[2 * win + 1:, :-1 - 2 * win] -
                    cum_params[:-1 - 2 * win, 2 * win + 1:] +
                    cum_params[:-1 - 2 * win, :-1 - 2 * win])
        del cum_params
        op_flow = np.zeros(im1.shape + (2,))
        det = win_params[...,0] * win_params[..., 1] - win_params[..., 2] **2
        op_flow_x = np.where(det != 0,
                            (win_params[..., 1] * win_params[..., 3] -
                            win_params[..., 2] * win_params[..., 4]) / det,
                            0)
        op_flow_y = np.where(det != 0,
                            (win_params[..., 0] * win_params[..., 4] -
                            win_params[..., 2] * win_params[..., 3]) / det,
                            0)
        op_flow[win + 1: -1 - win, win + 1: -1 - win, 0] = op_flow_x[:-1, :-1]
        op_flow[win + 1: -1 - win, win + 1: -1 - win, 1] = op_flow_y[:-1, :-1]
        return op_flow

#############################################################################################
class BACKEND:
    def __init__(self):
        self.filters = {'Gaussian': FILTER_GAUSSIAN('Gaussian')}
        for x in self.filters.values():
            x.init()
        self.filter = 'Gaussian'
        self.optical_flow = OPTICAL_FLOW()
        self.data_loaded = False
        self.img_count = 0

        self.falseColors = np.zeros((1024,3),dtype=np.uint8)
        for i in range(256):
            self.falseColors[i,0] = 255
            self.falseColors[256 + i,0] = 255 - i
            self.falseColors[256 + i,1] = i
            self.falseColors[256 + i + 256,1] = 255 - i
            self.falseColors[512 + i,2] = i
            self.falseColors[512 + i + 256,2] = 255
            self.falseColors[512 + i + 256,0] = i
            self.falseColors[512 + i + 256,1] = i

        self.falseColors[1023] = [255,255,255]

        self.img_0_pre = None
        self.img_1_pre = None

        self.roi = None
        self.color_scaling_X = 1.0
        self.color_scaling_Y = 1.0


    def load(self, path):
        path_split = os.path.splitext(path)

        pathname = os.path.dirname(path)

        # print('Load sequence of ...', path_split)
        split_pos = len(path_split[0])
        for i in range(len(path_split[0]) - 1, 0, -1):
            s = path_split[0][i:len(path_split[0])]
            if not s.isnumeric():
                break
            split_pos = i
        # digit_num = len(path_split[0]) - split_pos
        # print(path_split[0][0:split_pos] + ' , ' + path_split[0][split_pos:len(path_split[0])], digit_num)

        img_count = 0
        for file in os.listdir(pathname):
            f = pathname + '/' + file
            if f.startswith(path_split[0][0:split_pos]) & f.endswith(path_split[1]):
                img_count = img_count + 1

        names = [str] * img_count
        i = 0
        for file in os.listdir(pathname):
            f = pathname + '/' + file
            if f.startswith(path_split[0][0:split_pos]) & f.endswith(path_split[1]):
                names[i] = f
                i = i + 1

        names.sort()
        # print('Images found: ', img_count)
        self.images = [None] * img_count
        i = 0
        for _ in os.listdir(pathname):
            self.images[i] = cv2.imread(names[i], cv2.IMREAD_GRAYSCALE)
            # print('loading:', names[i])
            yield [i, img_count]
            i = i + 1

        self.width = self.images[0].shape[1]
        self.height = self.images[0].shape[0]

        self.optical_flow.init(self.width, self.height)
        #        cap = cv2.VideoCapture(path_split[0][0:split_pos] + '%d' + path_split[1])
        self.data_loaded = True
        self.img_count = img_count

        self.img_0_pre = None
        self.img_1_pre = None
        self.last_opt = None

        self.roi = None
        self.fulgrad = None
        self.optical_flow.opticalFlow_data = None
        self.optical_flow.opticalFlow_idx = 0

        return [i, img_count]

    def isDataLoaded(self):
        return self.data_loaded

    def getImgCount(self):
        return self.img_count

    def getFilters(self):
        return self.filters

    def getFilter(self):
        return self.filter

    def getImage(self, idx):
        idx = np.clip(idx,0,len(self.images) - 1)
        return self.images[idx]

    def getImageFiltered(self, idx):
        return self.filters[self.filter].process(self.getImage(idx))

    def getImageFlow(self, idx,direction,roi,strain_x = None):

        x_s = 0
        y_s = 0
        x_e = self.width
        y_e = self.height

        img_0 = self.getImage(idx)
        img_1 = self.getImage(idx + 1)
        img_0_c = cv2.cvtColor(img_0, cv2.COLOR_GRAY2BGR)


        if not np.array_equal(self.roi,roi) or (self.roi is None and roi is not None):
            print('roi not equal')
            self.fulgrad = roi
            self.optical_flow.opticalFlow_data = None
            self.optical_flow.opticalFlow_idx = 0

        self.roi = roi

        if roi:
            x_s,y_s,x_e,y_e = roi

        sigma = 3
        p0 = np.array([[[x, y]] for y in range(y_s - sigma, y_e + sigma, 1) for x in range(x_s - sigma, x_e + sigma, 1)],dtype=np.float32)
        p1, st, err = cv2.calcOpticalFlowPyrLK(img_0, img_1, p0, None, **self.optical_flow.lk_params)
        optical_flow = p1 - p0
        optical_flow.shape = (y_e - y_s + sigma * 2,x_e - x_s + sigma * 2,2)

        if self.optical_flow.opticalFlow_data is None:
            self.optical_flow.opticalFlow_data = np.zeros((self.optical_flow.accumulation,optical_flow.shape[0],optical_flow.shape[1],optical_flow.shape[2]))
            self.optical_flow.opticalFlow_idx = 0
            for i in range(self.optical_flow.opticalFlow_data.shape[0]):
                self.optical_flow.opticalFlow_data[i] = optical_flow
        else:
            self.optical_flow.opticalFlow_data[self.optical_flow.opticalFlow_idx] = optical_flow
            self.optical_flow.opticalFlow_idx = (self.optical_flow.opticalFlow_idx + 1) % self.optical_flow.opticalFlow_data.shape[0]
        
        optical_flow = np.sum(self.optical_flow.opticalFlow_data,axis=0)

        x = (optical_flow[sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s),0]) / self.optical_flow.opticalFlow_data.shape[0] # - np.median(optical_flow[:,:,1])) / self.optical_flow.opticalFlow_data.shape[0]
        y = (optical_flow[sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s),1]) / self.optical_flow.opticalFlow_data.shape[0] # - np.median(optical_flow[:,:,0])) / self.optical_flow.opticalFlow_data.shape[0]
#        x = x - np.median(x)
#        y = y - np.median(y)
        x = np.clip(np.abs(x) * 1024,0,1023).astype(np.int)
        y = np.clip(np.abs(y) * 1024,0,1023).astype(np.int)
        opt_flow_x = self.falseColors[x]
        opt_flow_y = self.falseColors[y]
        img_opt_x = img_0_c.copy()
        img_opt_y = img_0_c.copy()
        img_opt_x[y_s:y_e,x_s:x_e] = opt_flow_x
        img_opt_y[y_s:y_e,x_s:x_e] = opt_flow_y

        optical_flow_filter_y = self.filters[self.filter].process(optical_flow[:,:,1]) # gaussian_filter(optical_flow[:,:,1],sigma=sigma)
        optical_flow_filter_x = self.filters[self.filter].process(optical_flow[:,:,0]) # gaussian_filter(optical_flow[:,:,0],sigma=sigma)

        x = (optical_flow_filter_x[sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s)]) / self.optical_flow.opticalFlow_data.shape[0] #  - np.median((optical_flow_filter_x[sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s)])) / self.optical_flow.opticalFlow_data.shape[0]
        y = (optical_flow_filter_y[sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s)]) / self.optical_flow.opticalFlow_data.shape[0] # - np.median((optical_flow_filter_y[sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s)])) / self.optical_flow.opticalFlow_data.shape[0]

        x = np.clip(np.abs(x) * 1024,0,1023).astype(np.int)
        y = np.clip(np.abs(y) * 1024,0,1023).astype(np.int)
        opt_flow_filter_x = self.falseColors[x]
        opt_flow_filter_y = self.falseColors[y]
        img_opt_filter_x = img_0_c.copy()
        img_opt_filter_y = img_0_c.copy()
        img_opt_filter_x[y_s:y_e,x_s:x_e] = opt_flow_filter_x
        img_opt_filter_y[y_s:y_e,x_s:x_e] = opt_flow_filter_y

        grad_x = np.array(np.gradient(optical_flow_filter_x))
        grad_y = np.array(np.gradient(optical_flow_filter_y))
#        print(np.array(grad_x).shape)
#        print(np.array(grad_y).shape)
        fulgrad = np.sqrt(np.array(grad_x)**2 + np.array(grad_y)**2) #np.stack((np.array(grad_x)[0,:,:],np.array(grad_x)[1,:,:]),axis=2)#np.sqrt(grad[0]**2 + grad[1]**2)
#        print(fulgrad.shape)
        fulgrad = fulgrad[:,sigma:sigma+(y_e-y_s),sigma:sigma+(x_e-x_s)]
#        print(fulgrad.shape)

        img_strain_x = img_0_c.copy()
        img_strain_y = img_0_c.copy()
        img_strain_f = img_0_c.copy()

        x_st = np.clip(np.abs(fulgrad[0,:,:]) * 8192 * self.color_scaling_X,0,1023).astype(np.int)
        y_st = np.clip(np.abs(fulgrad[1,:,:]) * 8192 * self.color_scaling_Y,0,1023).astype(np.int)
        st_full = np.maximum.reduce([x_st,y_st])
        img_strain_x[y_s:y_e,x_s:x_e] = img_strain_x[y_s:y_e,x_s:x_e] * (1 - np.expand_dims(np.clip(x_st * 2,0,1023),axis=2) / 1023) + self.falseColors[x_st] * np.expand_dims(np.clip(x_st * 2,0,1023),axis=2) / 1023
        img_strain_y[y_s:y_e,x_s:x_e] = img_strain_y[y_s:y_e,x_s:x_e] * (1 - np.expand_dims(np.clip(y_st * 2,0,1023),axis=2) / 1023) + self.falseColors[y_st] * np.expand_dims(np.clip(y_st * 2,0,1023),axis=2) / 1023
#        img_strain_f[y_s:y_e,x_s:x_e] = img_strain_f[y_s:y_e,x_s:x_e] * (1 - np.expand_dims(np.clip(st_full * 2,0,1023),axis=2) / 1023) + self.falseColors[st_full] * np.expand_dims(np.clip(st_full * 2,0,1023),axis=2) / 1023

        return cv2.cvtColor(img_0, cv2.COLOR_GRAY2BGR),(img_opt_filter_x,img_opt_filter_y),(img_opt_x,img_opt_y),(img_strain_x,img_strain_y), fulgrad

    def export(self,path,start,end,summary):
        print("exporting to ",path,start,end)

        if summary == True:
            strain = []
            writer = cv2.VideoWriter(path + '/summary.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (self.width,self.height * 7))
            strain = np.ones((end - start, 2, self.height, self.width))
            for i in range(start,end):
                im_org, (img_opt_filter_x,img_opt_filter_y), (img_opt_x,img_opt_y),(img_strain_x,img_strain_y),fulgrad = self.getImageFlow(i,'x',self.roi)
                strain[i - start] = fulgrad
                im = np.vstack((im_org, img_opt_filter_x,img_opt_filter_y, img_opt_x,img_opt_y,img_strain_x,img_strain_y))
                print(i,im.shape)
                writer.write(im)
                yield [i-start, end-start]
            writer.release()
            strain_x = np.array([i for (i,_) in strain])
            strain_y = np.array([j for (_,j) in strain])
            np.savez_compressed(path + '/strain', a=strain)
        else:
            strain = []
            writer_org = cv2.VideoWriter(path + '/org.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (self.width,self.height))
            writer_of = cv2.VideoWriter(path + '/opticalFlow.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (self.width,self.height * 2))
            writer_of_filters = cv2.VideoWriter(path + '/opticalFlow_filters.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (self.width,self.height * 2))
            writer_strain = cv2.VideoWriter(path + '/strain.mp4', cv2.VideoWriter_fourcc('m', 'p', '4', 'v'), 30, (self.width,self.height * 2))
            frame = 0
            strain = np.ones((end - start, 2, self.height, self.width))
#            print(strain_x.shape)
#            print(strain_y.shape)
            for i in range(start,end):
                print(i)
                im_org, (img_opt_filter_x,img_opt_filter_y), (img_opt_x,img_opt_y),(img_strain_x,img_strain_y),fulgrad = self.getImageFlow(i,'x',self.roi)
                strain[i - start] = fulgrad
                frame += 1
                writer_org.write(im_org)
                writer_of.write(np.vstack((img_opt_x,img_opt_y)))
                writer_of_filters.write(np.vstack((img_opt_filter_x,img_opt_filter_y)))
                writer_strain.write(np.vstack((img_strain_x,img_strain_y)))
                yield [i-start, end-start]
            writer_strain.release()
            writer_of.release()
            writer_of_filters.release()
            writer_org.release()           
            print('strain shape:',strain.shape)
            np.savez_compressed(path + '/strain', a=strain)
        return [end-start, end-start]

        print('export done')
    
    def projectJSON(self,path,projectname,raw_path,roi,hist,time):
        if not path:
            return
        dir_name = path + "/" +projectname
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        else: 
            print("directory already exists, please rename project to save it")

        filter = self.getFilter()
        data = {
            'project_Title': projectname,
            'path': raw_path,
            'ROI_window': { 'coordinate description':'upper_left_corner_X, upper_left_corner_Y, lower_right_corner_X, lower_right_corner_Y',
                    'coordinates':roi
            },
            'timerange' : time,
            'raw_filter' : {
                'selected' : 'maybe differnet filters',
                'parameter' : None
            },
            'optical_flow' : {
                'selected' : 'Lukas Kanade',
                'parameter' : self.optical_flow.getParameters()
            },
            'filter_of' : {
                'selected' : str(filter),
                'parameter' : self.filters[self.filter].getParameters()
            },
            'strain' : {
                'selected' : 'maybe differnet strains',
                'parameter' : None
            },
            'historical_point' : hist
        }

        completeName_ = os.path.join(dir_name, "data.txt")
        with open( completeName_, 'w') as outfile:
            json.dump(data, outfile,  indent = 6)
            #json.dump(data1, outfile)
#############################################################################################
