from PySide2.QtCore import QRect, Qt, Signal, QPointF
from PySide2.QtGui import QPainter, QPen
from PySide2.QtWidgets import QLabel


class DrawLabel(QLabel):
    hist_x = 0
    hist_y = 0
    draw_hist = False
    histChanged = Signal(object)

    x0 = 0
    y0 = 0
    x1 = 0
    y1 = 0
    flag = False
    rectangleChanged = Signal(object)
    drawAllowed = False

    def mousePressEvent(self, event):
        if self.drawAllowed:
            self.flag = True
            self.x0 = event.x()
            self.y0 = event.y()
        if self.draw_hist:
            self.hist_x = event.x()
            self.hist_y = event.y()
            self.histChanged.emit((self.hist_x, self.hist_y))
            self.draw_hist = False

    def mouseReleaseEvent(self, event):
        if self.drawAllowed:
            self.flag = False
            self.rectangleChanged.emit((self.x0, self.y0, self.x1, self.y1))
            self.drawAllowed = False

    def mouseMoveEvent(self, event):
        if self.flag and self.drawAllowed:
            self.x1 = event.x()
            self.y1 = event.y()
            self.update()

    def paintEvent(self, event):
        super().paintEvent(event)
        rect = QRect(self.x0, self.y0, abs(self.x1 - self.x0), abs(self.y1 - self.y0))
        painter = QPainter(self)
        painter.setPen(QPen(Qt.red, 2, Qt.SolidLine))
        painter.drawRect(rect)

        if self.hist_x != 0 or self.hist_y != 0:
            painter2 = QPainter(self)
            painter2.setPen(QPen(Qt.blue, 2, Qt.SolidLine))
            painter2.drawEllipse(QPointF(self.hist_x, self.hist_y), 2, 2)

    def allowDraw(self, value):
        self.flag = False
        self.drawAllowed = value
        if value:
            self.draw_hist = False

    def setRectangle(self, x0, y0, x1, y1):
        self.x0 = x0
        self.x1 = x1
        self.y0 = y0
        self.y1 = y1
        self.update()

    def setHistPoint(self, x, y):
        self.hist_x = x
        self.hist_y = y
        self.update()

    def allowHist(self, value):
        self.draw_hist = value
        if value:
            self.allowDraw(False)

    def getRectangle(self):
        return self.x0, self.y0, self.x1, self.y1

    def getHistPoint(self):
        return self.hist_x, self.hist_y