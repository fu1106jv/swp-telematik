<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/BAM.png" alt="Logo" width="80" height="32.25">
  </a>

  <h3 align="center">BAM App</h3>

  <p align="center">
    An App for Calculating and Visualizing Optical Flow and Strain with several algorithms!
    <br />
  </p>
</div>
<br><br>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#run">Run</a></li>
        <li><a href="#how-to-build-a-version">How to Build a Version</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#sources">Sources</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

![Product Screen Shot](images/screenshot.png)

Some nice info about our Project

### Built With

* [Python](https://www.python.org)
* [OpenCV](https://opencv.org)
* [PySide2](https://pypi.org/project/PySide2/)

<!-- GETTING STARTED -->
## Getting Started

Either use the exe file in the ```/dist``` folder or use Python:
If you use python navigate into the ```/src``` folder with your shell 

### Prerequisites 

An installation of Python 3 is needed and the project dependencies:

  ```sh
  pip install requirements.txt
  ```
  if above does not work, try this:

  ```sh
  python -m pip install -r requirements.txt
  ```

### Run
  ```sh
  python main.py
  ```

<!-- ROADMAP -->

### How to Build a Version

First make sure all images are in the `images.qrc`

After changes run:

```sh
pyside2-rcc images.qrc -o rc_images.py
```

Create an Exe with PyInstaller:
```sh
pyinstaller --name="BAMAPP" --windowed --onefile --noconsole main.py
```


## Roadmap

* Strain Calulation
* Export
* Save
* Choose a Icon for the APP
* App Name

<!-- LICENSE -->
## License

<!--Distributed under the MIT License. See `LICENSE` for more information. -->

<!-- CONTACT -->
## Contact

## Sources

<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com </a></div>